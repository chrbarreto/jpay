package br.com.chrbarreto.jpay.model;

public class ResponseBody {

    private boolean success = true;
    private Payload payload = new Payload();

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Payload getPayload() {
        return this.payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

}
