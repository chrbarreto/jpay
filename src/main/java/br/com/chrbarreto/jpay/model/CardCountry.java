package br.com.chrbarreto.jpay.model;

public class CardCountry {
    private Integer numeric, latitude, longitude;
    private String alpha2, name, emoji, currency;

    public Integer getNumeric() {
        return this.numeric;
    }

    public void setNumeric(Integer numeric) {
        this.numeric = numeric;
    }

    public Integer getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public String getAlpha2() {
        return this.alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmoji() {
        return this.emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
