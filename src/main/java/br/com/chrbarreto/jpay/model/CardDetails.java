package br.com.chrbarreto.jpay.model;

public class CardDetails {
    private String scheme;
    private String type;
    private String brand;
    private Boolean prepaid;

    private CardNumber number;
    private CardCountry country;
    private CardBank bank;

    public String getScheme() {
        return this.scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Boolean getPrepaid() {
        return this.prepaid;
    }

    public void setPrepaid(Boolean prepaid) {
        this.prepaid = prepaid;
    }

    public CardNumber getNumber() {
        return this.number;
    }

    public void setNumber(CardNumber number) {
        this.number = number;
    }

    public CardCountry getCountry() {
        return this.country;
    }

    public void setCountry(CardCountry country) {
        this.country = country;
    }

    public CardBank getBank() {
        return this.bank;
    }

    public void setBank(CardBank bank) {
        this.bank = bank;
    }

}
