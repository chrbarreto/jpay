package br.com.chrbarreto.jpay.model;

public class Payload {

    private String scheme;
    private String type;
    private String bank;

    public Payload() {
        super();
    }

    public Payload(String scheme, String type, String bank) {
        this();
        this.scheme = scheme;
        this.type = type;
        this.bank = bank;
    }

    public String getScheme() {
        return this.scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBank() {
        return this.bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

}
