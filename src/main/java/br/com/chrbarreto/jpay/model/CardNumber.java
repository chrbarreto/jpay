package br.com.chrbarreto.jpay.model;

public class CardNumber {
    private Integer length;
    private Boolean luhn;

    public Integer getLength() {
        return this.length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Boolean getLuhn() {
        return this.luhn;
    }

    public void setLuhn(Boolean luhn) {
        this.luhn = luhn;
    }

}
